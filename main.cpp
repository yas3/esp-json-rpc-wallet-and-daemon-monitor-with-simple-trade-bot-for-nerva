#include <Arduino.h>
#include "SSD1306.h"
#include <HTTPClient.h>
#include <WiFiClientSecure.h>
#include <ArduinoJson.h>
#include "time.h"
#include "header.h"
#include <EEPROM.h>

const char *ssid = "";        // your network SSID (name of wifi network)
const char *password = ""; // your network password
String TRADE_API_KEY = ":";
String wallet_name = ""; //RPC wallet
String wallet_password = "";
double sell_val = 0.00000220;
double buy_val = 0.00000200;
double invest = 200;

String miner_ip = "";
String wallet_ip = "";

int miner_port = 17566;
int wallet_port = 19566;

SSD1306 display(0x3c, 5, 4);
DynamicJsonDocument doc(2048);
enum state
{
  sell,
  buy
};
state s1 = buy;
byte sell_level = 0;
byte buy_level = 0;
uint8_t ledPin = 16; // Onboard LED reference

WiFiClient client;
String response;
double nerva_price = 0;
double balance;
double BTC_price;
double value;
long height;
long hashrate;
double nerva_balance_trade;
double nerva_available_trade;
double btc_balance_trade;
double btc_available_trade;
double bid;
double ask;

void wifiConnect()
{
  digitalWrite(ledPin, LOW);
  WiFi.begin(ssid, password);
  WiFi.mode(WIFI_STA);
  display.clear();
  display.drawXbm(0, 0, WiFi_Logo_width, WiFi_Logo_height, WiFi_Logo_bits);
  display.display();
  delay(1000);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
  }
  display.clear();
  display.display();
  digitalWrite(ledPin, HIGH);
}

void initDisplay()
{
  display.init();
  display.setFont(ArialMT_Plain_10); // does what is says
  display.setTextAlignment(TEXT_ALIGN_LEFT);
}
int updateBTCPrice()
{
  int status = 0;
  //DynamicJsonDocument doc(2048);
  WiFiClientSecure *client = new WiFiClientSecure;
  if (client)
  {
    client->setCACert(caCert2);
    HTTPClient https;
    if (https.begin(*client, "https://api.blockchain.info/stats"))
    { // HTTPS

      int httpCode = https.GET();

      if (httpCode > 0)
      {
        //Serial.printf("[HTTPS] GET... code: %d\n", httpCode);

        if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY)
        {
          String payload = https.getString();
          //Serial.println(payload);
          DeserializationError error = deserializeJson(doc, payload.c_str());
          if (error)
          {
            Serial.print(F("deserializeJson() failed: "));
            Serial.println(error.c_str());
          }
          else
          {
            if (doc.containsKey("market_price_usd"))
            {
              BTC_price = doc["market_price_usd"];
              status = 1;
            }
          }
        }
      }
      https.end();
    }
  }
  return status;
}

void updateBalance(const char *currency, double *balance, double *available)
{
  //DynamicJsonDocument doc(2048);
  WiFiClientSecure *client = new WiFiClientSecure;
  if (client)
  {
    client->setCACert(caCert);
    HTTPClient https;
    if (https.begin(*client, "https://" + TRADE_API_KEY + "@tradeogre.com/api/v1/account/balance"))
    { // HTTPS

      https.addHeader("Content-Type", "application/x-www-form-urlencoded"); //Specify content-type header

      int httpCode = https.POST("currency=" + String(currency)); //Send the request

      if (httpCode > 0)
      {
        //Serial.printf("[HTTPS] GET... code: %d\n", httpCode);

        if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY)
        {
          String payload = https.getString();
          //Serial.println(payload);
          DeserializationError error = deserializeJson(doc, payload.c_str());
          if (error)
          {
            Serial.print(F("deserializeJson() failed: "));
            Serial.println(error.c_str());
          }
          else
          {
            if (doc.containsKey("balance"))
            {
              const char *balance_str = doc["balance"];
              *balance = atof(balance_str);
            }
            if (doc.containsKey("available"))
            {
              const char *avail_str = doc["available"];
              *available = atof(avail_str);
            }
          }
        }
      }
      https.end();

    }
    delete client;
  }
  else
  {
    Serial.println("Unable to create client");
  }

  Serial.println();
}

int updateNervaPrice()
{
  int status = 0;
  //DynamicJsonDocument doc(2048);
  WiFiClientSecure *client = new WiFiClientSecure;
  if (client)
  {
    client->setCACert(caCert);
    HTTPClient https;
    if (https.begin(*client, "https://tradeogre.com/api/v1/ticker/BTC-XNV"))
    { // HTTPS
      int httpCode = https.GET();
      if (httpCode > 0)
      {
        //Serial.printf("[HTTPS] GET... code: %d\n", httpCode);

        if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY)
        {
          String payload = https.getString();
          //Serial.println(payload);
          DeserializationError error = deserializeJson(doc, payload.c_str());
          if (error)
          {
            Serial.print(F("deserializeJson() failed: "));
            Serial.println(error.c_str());
          }
          else
          {
            if (doc.containsKey("price"))
            {
              const char *price_str = doc["price"];
              nerva_price = atof(price_str);
            }
            if (doc.containsKey("bid"))
            {
              const char *bid_str = doc["bid"];
              bid = atof(bid_str);
            }
            if (doc.containsKey("ask"))
            {
              const char *ask_str = doc["ask"];
              ask = atof(ask_str);
              status = 1;
            }
          }
        }
      }
      https.end();
    }
    delete client;
  }
  return status;
}

int send_daemon(String msg)
{
  HTTPClient http;
  http.setTimeout(1000);
  http.setConnectTimeout(1000);
  int httpCode = 0;
  if (http.begin(client, miner_ip, miner_port, "/" + msg))
  {
    http.addHeader("Content-Type", "application/json");
    httpCode = http.POST("");
    if (httpCode > 0)
    {
      if (httpCode == HTTP_CODE_OK)
      {
        response = http.getString();
      }
    }
  }
  return httpCode;
}

int send(String payload)
{
  HTTPClient http;
  http.setTimeout(3000);
  http.setConnectTimeout(2000);
  int httpCode = 0;
  if (http.begin(client, wallet_ip, wallet_port, "/json_rpc"))
  {
    http.addHeader("Content-Type", "application/json");
    httpCode = http.POST(payload);
    if (httpCode > 0)
    {
      if (httpCode == HTTP_CODE_OK)
      {
        response = http.getString();
      }
    }else
    {
      Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }

  }
  return httpCode;
}
int open_wallet()
{
  doc["jsonrpc"] = "2.0";
  doc["id"] = "0";
  doc["method"] = "open_wallet";
  doc["params"]["filename"] = wallet_name;
  doc["params"]["password"] = wallet_password;

  String payload;
  serializeJson(doc, payload);
  if (send(payload) == HTTP_CODE_OK)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

void get_balance()
{
  balance = 0;
  //DynamicJsonDocument doc(1024);
  doc["jsonrpc"] = "2.0";
  doc["id"] = "0";
  doc["method"] = "get_balance";
  doc["params"]["account_index"] = 0;
  String payload;
  serializeJson(doc, payload);
  if (send(payload) == HTTP_CODE_OK)
  {
    DeserializationError err = deserializeJson(doc, response);
    if (!err && doc.containsKey("result"))
    {
      long long balance_l = doc["result"]["balance"];
      balance = (double)balance_l / 1000000000000.0;
    }
    if(!err && doc.containsKey("error")){//try to open the wallet again
      open_wallet();
    }
  }
}

void get_height()
{
  height = 0;
  //DynamicJsonDocument doc(1024);
  doc["jsonrpc"] = "2.0";
  doc["id"] = "0";
  doc["method"] = "get_height";
  String payload;
  serializeJson(doc, payload);
  if (send(payload) == HTTP_CODE_OK)
  {
    DeserializationError err = deserializeJson(doc, response);
    if (!err && doc.containsKey("result"))
    {
      height = doc["result"]["height"];
    }
  }
}

void get_hashrate()
{
  hashrate = 0;
  //DynamicJsonDocument doc(1024);
  if (send_daemon("mining_status") == HTTP_CODE_OK)
  {
    DeserializationError err = deserializeJson(doc, response);
    if (!err && doc.containsKey("speed"))
    {
      hashrate = doc["speed"];
    }
  }
}
void drawImageDemo()
{
  // see http://blog.squix.org/2015/05/esp8266-nodemcu-how-to-create-xbm.html
  // on how to create xbm files
  display.drawXbm(64, 0, nerva_width, nerva_height, nerva_bits);
}

void buyNerva(double qty, double price)
{
  //DynamicJsonDocument doc(2048);
  WiFiClientSecure *client = new WiFiClientSecure;
  if (client)
  {
    client->setCACert(caCert);
    HTTPClient https;
    if (https.begin(*client, "https://" + TRADE_API_KEY + "@tradeogre.com/api/v1/order/buy"))
    {                                                                       // HTTPS
      https.addHeader("Content-Type", "application/x-www-form-urlencoded"); //Specify content-type header
      String payload = "market=BTC-XNV&quantity=" + String(qty, 8) + "+&price=" + String(price, 8);
      //Serial.println(payload);
      int httpCode = https.POST(payload); //Send the request

      if (httpCode > 0)
      {
        //Serial.printf("[HTTPS] GET... code: %d\n", httpCode);

        if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY)
        {
          String payload = https.getString();
          DeserializationError error = deserializeJson(doc, payload.c_str());
          if (error)
          {
            Serial.print(F("deserializeJson() failed: "));
            Serial.println(error.c_str());
          }
          else
          {
            if (doc.containsKey("sucess"))
            {
              const char *status_str = doc["success"];
              Serial.println(status_str);
            }
          }
        }
      }
      https.end();
    }
    delete client;
  }
  Serial.println();
}

void sellNerva(double qty, double price)
{
  WiFiClientSecure *client = new WiFiClientSecure;
  if (client)
  {
    client->setCACert(caCert);
    HTTPClient https;
    if (https.begin(*client, "https://" + TRADE_API_KEY + "@tradeogre.com/api/v1/order/sell"))
    {                                                                       // HTTPS
      https.addHeader("Content-Type", "application/x-www-form-urlencoded"); //Specify content-type header
      String payload = "market=BTC-XNV&quantity=" + String(qty, 8) + "+&price=" + String(price, 8);
      //Serial.println(payload);
      int httpCode = https.POST(payload); //Send the request

      if (httpCode > 0)
      {
        if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY)
        {
          String payload = https.getString();
          DeserializationError error = deserializeJson(doc, payload.c_str());
          if (error)
          {
            Serial.print(F("deserializeJson() failed: "));
            Serial.println(error.c_str());
          }
          else
          {
            if (doc.containsKey("sucess"))
            {
              const char *status_str = doc["success"];
              Serial.println(status_str);
            }
          }
        }
      }
      https.end();
    }
    delete client;
  }
}
void restoreData()
{
  if (EEPROM.read(0) != 255)
  {
    int status = EEPROM.read(0);
    if (status == 0)
    {
      s1 = sell;
    }
    else
    {
      s1 = buy;
    }
  }
  else
  {
    EEPROM.write(0, 0);
    EEPROM.commit();
    delay(10);
  }

  if (EEPROM.read(1) != 255)
  {
    sell_level = EEPROM.read(1);
  }
  else
  {
    EEPROM.write(1, 0);
    EEPROM.commit();
    delay(10);
    sell_level = EEPROM.read(1);
  }
  if (EEPROM.read(2) != 255)
  {
    buy_level = EEPROM.read(2);
  }
  else
  {
    EEPROM.write(2, 0);
    EEPROM.commit();
    delay(10);
    buy_level = EEPROM.read(2);
  }
  Serial.print("State: ");
  Serial.println(s1);
  Serial.print("buy lvl: ");
  Serial.println(buy_level);
  Serial.print("sell lvl: ");
  Serial.println(sell_level);
}
void displayStatus(int status, int row)
{
  if (status)
  {
    display.drawString(100, row, String("OK"));
  }
  else
  {
    display.drawString(100, row, String("Failed"));
  }
  display.display();
}
void displayInfo(String msg, int row)
{
  display.drawString(0, row, String(msg));
  display.display();
}
void updateValue(){
  value = nerva_price * BTC_price * (balance + nerva_balance_trade) + BTC_price * btc_balance_trade;
}

void setup()
{
  pinMode(ledPin, OUTPUT);
  EEPROM.begin(3);
  Serial.begin(115200);
  initDisplay();
  delay(1000);
  display.clear();
  display.display();
  wifiConnect();
  displayInfo("opening wallet", 10);
  int status = open_wallet();
  displayStatus(status, 10);
  displayInfo("updating nerva price", 20);
  status = updateNervaPrice();
  displayStatus(status, 20);
  displayInfo("updating btc price", 30);
  status = updateBTCPrice();
  displayStatus(status, 30);
  displayInfo("updating balance", 40);
  updateBalance("XNV", &nerva_balance_trade, &nerva_available_trade);
  updateBalance("BTC", &btc_balance_trade, &btc_available_trade);
  get_balance();
  updateValue();
  restoreData();
  client.setTimeout(2000);
}

void trade()
{
  updateNervaPrice();
  btc_balance_trade = 0;
  btc_available_trade = 0;
  nerva_available_trade = 0;
  nerva_balance_trade = 0;

  updateBalance("XNV", &nerva_balance_trade, &nerva_available_trade);
  updateBalance("BTC", &btc_balance_trade, &btc_available_trade);

  if (nerva_available_trade > invest)
  {
    if (bid > sell_val * 1.2 && sell_level <= 2)
    { //sell at threshold * 120%
      sellNerva(invest, bid);
      s1 = sell;
      sell_level = 3;
      buy_level = 0;
      EEPROM.write(0, 0);
      EEPROM.write(1, sell_level);
      EEPROM.write(2, buy_level);
      EEPROM.commit();
      delay(10);
      Serial.println("==Sell==");
      Serial.println(bid, 8);
    }

    if (bid > sell_val * 1.1 && sell_level <= 1)
    { //sell at threshold * 110%
      sellNerva(invest, bid);
      s1 = sell;
      sell_level = 2;
      buy_level = 0;
      EEPROM.write(0, 0);
      EEPROM.write(1, sell_level);
      EEPROM.write(2, buy_level);
      EEPROM.commit();
      delay(10);
      Serial.println("==Sell==");
      Serial.println(bid, 8);
    }

    if (bid > sell_val && s1 == buy)
    { //sell at sell threshold
      sellNerva(invest, bid - 0.00000001);
      s1 = sell;
      sell_level = 1;
      buy_level = 0;
      EEPROM.write(0, 0);
      EEPROM.write(1, sell_level);
      EEPROM.write(2, buy_level);
      EEPROM.commit();
      delay(10);
      Serial.println("==Sell==");
      Serial.println(bid, 8);
    }
  }
  //buy
  if (btc_available_trade / ask > invest)
  {
    if (ask < buy_val * 0.8 && buy_level <= 2)
    {
      buyNerva(invest, ask);
      s1 = buy;
      sell_level = 0;
      buy_level = 3;
      EEPROM.write(0, 1);
      EEPROM.write(1, sell_level);
      EEPROM.write(2, buy_level);
      EEPROM.commit();
      delay(10);
      Serial.println("==Buy==");
      Serial.println(ask);
    }

    if (ask < buy_val * 0.9 && buy_level <= 1)
    {
      buyNerva(invest, ask);
      s1 = buy;
      sell_level = 0;
      buy_level = 2;
      EEPROM.write(0, 1);
      EEPROM.write(1, sell_level);
      EEPROM.write(2, buy_level);
      EEPROM.commit();
      delay(10);
      Serial.println("==Buy==");
      Serial.println(ask);
    }

    if (ask < buy_val && nerva_price != 0 && s1 == sell)
    {
      buyNerva(invest, ask + 0.00000001);
      s1 = buy;
      sell_level = 0;
      buy_level = 1;
      EEPROM.write(0, 1);
      EEPROM.write(1, sell_level);
      EEPROM.write(2, buy_level);
      EEPROM.commit();
      delay(10);
      Serial.println("==Buy==");
      Serial.println(ask);
    }
  }
}


void loop()
{
  static unsigned long timer_rpc = 10000UL;
  if (millis() - timer_rpc > 10000UL)
  {
    get_balance();
    get_height();
    get_hashrate();
    updateValue();
    timer_rpc = millis();
  }

  static unsigned long timer_update = 0;
  if (millis() - timer_update > 60000UL)
  {
    get_balance();
    trade();
    updateValue();
    timer_update = millis();
  }

  static unsigned long timer_display = 60000UL;
  if (millis() - timer_display > 10000UL)
  {
    display.clear();
    display.drawString(0, 0, String(BTC_price, 0) + " $/BTC");
    display.drawString(0, 10, String(nerva_price * 100000000, 0) + " Sat/XNV");
    display.drawString(0, 20, String(balance + nerva_balance_trade, 1) + " XNV");
    display.drawString(0, 30, String(btc_balance_trade, 6) + "BTC");
    display.drawString(0, 40, String(value, 2) + " USD");
    //display.drawString(0, 40, "H:"+String(height));
    display.drawString(0, 50, String(hashrate / 1000.0, 2) + "kH/s");
    drawImageDemo();
    display.display();
    timer_display = millis();
  }

  static unsigned long timer_ticker = 0;
  if (millis() - timer_ticker > 120000UL)
  {

    updateBTCPrice();

    timer_ticker = millis();
  }
}